// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:shop_fast/main.dart';
import 'package:shop_fast/pages/login.dart';
//this is my own package import

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: const Text(
          "Zbank",
          style: TextStyle(
            color: Colors.red,
          ),
        ),
      ),

      drawer: Drawer(
        backgroundColor: Colors.blue,
        child: Padding(
          padding: const EdgeInsets.only(right: 0.5),
          child: ListView(
            children: <Widget>[
//            header
              UserAccountsDrawerHeader(
                margin: EdgeInsets.all(0.5),
                accountName: Text('Udah Paul'),
                accountEmail: Text('zachtech56@gmail.com'),
                currentAccountPicture: GestureDetector(
                  child: const CircleAvatar(
                    backgroundColor: Colors.blueGrey,
                    child: Icon(Icons.person, color: Colors.black),
                  ),
                ),
                decoration: const BoxDecoration(color: Colors.red),
              ),
//            Body
              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text('Home Page'),
                  leading: Icon(
                    Icons.home,
                    color: Colors.black,
                  ),
                ),
              ),

              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text('Transaction History'),
                  leading: Icon(
                    Icons.history,
                    color: Colors.black,
                  ),
                ),
              ),

              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text('Tranfer Funds'),
                  leading: Icon(Icons.money,
                   color: Colors.black),
                ),
              ),

              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text('Deposit'),
                  leading: Icon(Icons.account_balance_wallet,
                   color: Colors.black),
                ),
              ),

              Divider(),

              InkWell(
                onTap: () {},
                child: ListTile(
                  title: Text('Settings'),
                  leading: Icon(Icons.settings,
                  color: Colors.black,),
                ),
              ),

              InkWell(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Login()));
                },
                child: ListTile(
                  title: Text('Logout'),
                  leading: Icon(Icons.logout_outlined, color: Colors.red),
                ),
              )
            ],
          ),
        ),
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   items: [
      //     // Icon(Icon: Icons.back_hand),
      //   ],
      //   backgroundColor: Colors.white,

      // ),

      body: Stack(
        children: [
          Container(
            height: 202.0,
            margin: const EdgeInsets.fromLTRB(20.0, 25.0, 20.0, 20.0),
            color: Colors.blue,
          ),

          Container(
            color: Colors.red,
            margin: const EdgeInsets.only(top: 250.0),
          ),
        ],
      ),

    );
  }
}
