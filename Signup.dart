// ignore_for_file: prefer_final_fields, non_constant_identifier_names

// import 'package:Zbank/pages/login.dart';
import 'package:flutter/material.dart';
import 'package:shop_fast/pages/login.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:google_sign_in/google_sign_in.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:a_class/pages/home.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  // final GoogleSignIn googleSignIn = GoogleSignIn();
  // final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  //
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();
  TextEditingController _accountnumberController = TextEditingController();

  get Loading => false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: const Text(
          "Zbank",
          style: TextStyle(
            color: Colors.red,
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            color: Colors.blue,
            margin: const EdgeInsets.only(top: 0.5),
            child: Center(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                  child: ListView(
                    children: [

                      //Account Number
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.white.withOpacity(0.5),
                          elevation: 0.0,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: TextFormField(
                              controller: _accountnumberController,
                              decoration: const InputDecoration(
                                hintText: "Account Number",
                                icon: Icon(Icons.person),
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Field cannot be empty";
                                } else if (value.length < 6) {
                                  return "Phone Number is Required";
                                }
                              },
                            ),
                          ),
                        ),
                      ),

                      //Password
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.white.withOpacity(0.5),
                          elevation: 0.0,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: TextFormField(
                              controller: _passwordTextController,
                              decoration: const InputDecoration(
                                hintText: "Password",
                                icon: Icon(Icons.lock),
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Field cannot be empty";
                                } else if (value.length < 6) {
                                  return "Password must be at least 6 characters long";
                                }
                              },
                            ),
                          ),
                        ),
                      ),

                      //Email Address
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.white.withOpacity(0.5),
                          elevation: 0.0,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: TextFormField(
                              controller: _emailTextController,
                              decoration: const InputDecoration(
                                hintText: "Email",
                                icon: Icon(Icons.email),
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  Pattern pattern =
                                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                  RegExp regex = new RegExp('pattern');
                                  if (!regex.hasMatch(value))
                                    return 'Please enter a valid email';
                                  else
                                    return null;
                                }
                              },
                            ),
                          ),
                        ),
                      ),

                      //SignUp
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.red.withOpacity(1.0),
                          elevation: 0.0,
                          child: MaterialButton(
                            onPressed: () {},
                            minWidth: MediaQuery.of(context).size.width,
                            child: const Text(
                              "Sign up",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22.0,
                                  fontStyle: FontStyle.italic),
                            ),
                          ),
                        ),
                      ),

                      //Asking for account
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: RichText(
                            text: const TextSpan(
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 16.0),
                              children: [
                                TextSpan(text: "Already have an account?"),
                              ],
                            ),
                          ),
                        ),
                      ),

                      //Login
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.red.withOpacity(1.0),
                          elevation: 0.0,
                          child: MaterialButton(
                            onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Login()));
                            },
                            minWidth: MediaQuery.of(context).size.width,
                            child: const Text(
                              "SignIn",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22.0,
                                  fontStyle: FontStyle.italic),
                            ),
                          ),
                        ),
                        ),
                      
                    ],
                  ),
                ),
              ),
            ),
          ),
          Visibility(
            visible: Loading,
            child: Center(
              child: Container(
                alignment: Alignment.center,
                color: Colors.white.withOpacity(1.0),
                child: const CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.redAccent),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
