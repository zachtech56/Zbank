// ignore_for_file: deprecated_member_use, unnecessary_new, prefer_const_constructors, prefer_final_fields, non_constant_identifier_names, unused_local_variable

// import 'dart:html';

// import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shop_fast/pages/Signup.dart';
import 'package:shop_fast/pages/home.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:google_sign_in/google_sign_in.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';


class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  // final GoogleSignIn googleSignIn = GoogleSignIn();
  // final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  TextEditingController _accountTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();

  // late SharedPreferences preferences;
  bool Loading = false;
  bool isLogedin = false;

  var _auth;

  @override
  // void initState() {
  //   super.initState();
  //   isSignedIn();
  // }

  // void isSignedIn() async {
  //   setState(() {
  //     Loading = true;
  //   });
  //
  //   preferences = await SharedPreferences.getInstance();
  //   isLogedin = await googleSignIn.isSignedIn();
  //
  //   if (isLogedin) {
  //     Navigator.pushReplacement(
  //         context, MaterialPageRoute(builder: (context) => HomePage()));
  //   }
  //
  //   setState(() {
  //     Loading = false;
  //   });
  // }

  // Future handleSignIn() async {
  //   preferences = await SharedPreferences.getInstance();
  //
  //   setState(() {
  //     Loading = true;
  //   });
  //
  //   final GoogleSignInAccount? account = await googleSignIn.signIn();
  //   final GoogleSignInAuthentication authentication =
  //       await account!.authentication;
  //
  //   final OAuthCredential credential = GoogleAuthProvider.credential(
  //       idToken: authentication.idToken,
  //       accessToken: authentication.accessToken);
  //
  //   final UserCredential authResult =
  //       await _auth.signInWithCredential(credential);
  //   final User? user = authResult.user;
  //
  //   return user;
  //
  //   // if (User != null) {
  //   //   final QuerySnapshot result = await Firestore.instance
  //   //       .collection("users")
  //   //       .where("id", isEqualTo: User.uid)
  //   //       .getDocuments();
  //   //   final List<DocumentSnapshot> documents = result.documents;
  //
  //   //   if (documents.length == 0) {
  //   //     //  insert the user to out collection
  //   //     Firestore.instance
  //   //         .collection("users")
  //   //         .document(User.uid)
  //   //         .setData({
  //   //       "id": User.uid,
  //   //       "Username": User.displayName,
  //   //       "profilePicture": User.photoUrl
  //   //     });
  //
  //   //     await preferences.setString("id", User.uid);
  //   //     await preferences.setString("Username", User.displayName);
  //   //     await preferences.setString("photoUrl", User.displayName);
  //   //   }
  //   // } else {
  //   //     // await preferences.setString("id", documents[0]['id']);
  //   //     // await preferences.setString("Username", documents[0]['username']);
  //   //     // await preferences.setString("photoUrl", documents[0]['photoUrl']);
  //   // }
  //   // Fluttertoast.showToast(msg: "Login was successful");
  //   // setState(() {
  //   //   Loading = false;
  //   // });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: const Text(
          "Zbank",
          style: TextStyle(color: Colors.red),
        ),

      ),
      body: Stack(
        children: [

         Container(
           color: Colors.blue,
            margin: EdgeInsets.only(top: 0.5),
            child: Center(
              child: Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.only(top: 90.0),
                  child: ListView(
                    children: [

                      //Request for account Number
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(30.0),
                          color: Colors.white.withOpacity(0.5),
                          elevation: 0.0,
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: TextFormField(
                              controller: _accountTextController,
                              decoration: const InputDecoration(
                                hintText: "Enter Account Number",
                                icon: Icon(Icons.account_circle),
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  Pattern pattern =
                                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                  RegExp regex = new RegExp('pattern');
                                  if (!regex.hasMatch(value)) {
                                    return 'Please enter a valid email';
                                  } else {
                                    return null;
                                  }
                                }
                              },
                            ),
                          ),
                        ),
                      ),

                      //Password
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(25.0),
                          color: Colors.white.withOpacity(0.5),

                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: TextFormField(
                              controller: _passwordTextController,
                              decoration: const InputDecoration(
                                hintText: " Enter Password",
                                icon: Icon(Icons.lock),
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Field cannot be empty";
                                } else if (value.length < 6) {
                                  return "Password must be at least 6 characters long";
                                }
                              },
                            ),
                          ),
                        ),
                      ),

                      //Signin Button
                      Padding(
                        padding: const EdgeInsets.all(25.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.red.withOpacity(1.0),
                          elevation: 10.0,
                          child: MaterialButton(
                            onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomePage()));
                            },
                            minWidth: MediaQuery.of(context).size.width,
                            child: const Text(
                              "SignIn",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22.0,
                                  fontStyle: FontStyle.italic),
                            ),
                          ),
                        ),
                      ),

                      //Forget Password
                      const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "Forget Password",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
                        ),
                      ),

                      // Signup Button
                      Padding(
                        padding: EdgeInsets.all(25.0),
                        child: Material(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.red.withOpacity(1.0),
                          elevation: 10.0,
                          child: MaterialButton(
                            onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const SignUp()));
                            },
                            minWidth: MediaQuery.of(context).size.width,
                            child: const Text(
                              "SignUp",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 22.0,
                                  fontStyle: FontStyle.italic),
                            ),
                          ),
                        ),
                      ),


                    ],
                  ),
                ),
              ),
            ),
          ),
          Visibility(
            visible: Loading,
            child: Center(
              child: Container(
                alignment: Alignment.center,
                color: Colors.white.withOpacity(1.0),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.redAccent),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
